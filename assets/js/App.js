import React, { Component } from 'react';
import ApplicationForm from './application-form/ApplicationForm';
import './App.less';

class App extends Component {
    render() {
        return (
            <>
                <div>
                    <h1>
                        ci-base-app
                    </h1>
                </div>
                <div className="form1">
                    <ApplicationForm />
                </div>
                <div className="form1">
                    <ApplicationForm application="test123" />
                </div>
                <div className="form1">
                    <ApplicationForm application="something" />
                </div>
            </>
        );
    }
}

export default App;