#!/bin/bash
# https://github.com/docker-library/wordpress/issues/205#issuecomment-278319730
echo "php-init.sh >> start"

# exec ls -la "/etc/apache2/mods-enabled/"

echo "simbolic link for mod-rewrite - START"

# pwd

# https://stackoverflow.com/questions/10144634/htaccess-invalid-command-rewriteengine-perhaps-misspelled-or-defined-by-a-m
ln -s /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/rewrite.load

echo "simbolic link for mod-rewrite - END"

# exec ls -la "/etc/apache2/mods-enabled/"

echo "php-init.sh >> end"

# execute apache
exec "apache2-foreground"