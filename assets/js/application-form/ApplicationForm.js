import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import styles from './ApplicationForm.less';

const FormSchema = Yup.object().shape({
    name: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    email: Yup.string()
        .email('Invalid email')
        .required('Required'),
});

class ApplicationForm extends Component {
    // constructor(props) {
    //     super(props);
    //     this.state = {
            
    //     };
    // }
    render() {
        console.log('styles', styles);
        return (
            <>
                <div className={ styles.form }>
                    this is form for { this.props.application }
                </div>
                <Formik
                    initialValues={
                        {
                            name: '',
                            email: ''
                        }
                    }
                    validationSchema={ FormSchema }
                    onSubmit={
                        (values, { setSubmitting }) => {
                            console.log(values);
                            setTimeout(() => {
                                alert(JSON.stringify(values, null, 2));
                                setSubmitting(false);
                            }, 400);
                        }
                    }>
                    {
                        ({ isSubmitting, isValidating }) => (
                            <Form>
                                <Field
                                    type="text"
                                    name="name" />
                                <ErrorMessage
                                    name="name"
                                    className="name-error"
                                    component="div" />
                                <Field
                                    type="email"
                                    name="email" />
                                <ErrorMessage
                                    name="email"
                                    className="email-error"
                                    component="div" />
                                {
                                    isSubmitting &&
                                    <div>isSubmitting</div>
                                }
                                {
                                    isValidating &&
                                    <div>isValidating</div>
                                }
                                <button type="submit" disabled={isSubmitting}>
                                    Submit
                                </button>
                            </Form>
                        )
                    }
                </Formik>
            </>
        );
    }
}

ApplicationForm.propTypes = {
    application: PropTypes.string.isRequired,
};

ApplicationForm.defaultProps = {
    application: 'test',
};

export default ApplicationForm;