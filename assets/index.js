import React from 'react';
import ReactDOM from 'react-dom';
import App from './js/App';

import './less/main.less';
// import './less/main.less';

import WebFont from 'webfontloader';

console.log("hello12345");
// alert("hello");

WebFont.load({
    google: {
        // families: ['Titillium Web:300,400,700', 'sans-serif']
        families: ['Rubik', 'sans-serif']
    }
});

const domContainer = document.getElementById("root");

if (domContainer) {
    console.log('dom element exists');
    ReactDOM.render(
        <App />,
        domContainer
    );
}
