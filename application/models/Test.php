<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Model {
    public function getAll()
    {
        $this->db->select()->from('test');
        return $this->db->get();
    }

    public function get($id)
    {
        $this->db->select()->from('test')->where('id', $id);
        return $this->db->get();
    }
}