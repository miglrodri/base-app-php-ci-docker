# Bootstrap app with PHP(codeigniter) + Apache + mySQL + browser-sync

start docker services and run browser-sync
-
`npm run start:dev`

stop services
-
`npm run stop`

portainer
https://hub.docker.com/r/portainer/portainer/
`$ docker volume create portainer_data
$ docker run -d -p 9000:9000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer`

docker-compose-lamp
https://github.com/sprintcube/docker-compose-lamp